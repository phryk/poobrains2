import peewee

from rendering import Renderable


db_proxy = peewee.Proxy()


class StorageError(Exception):
    pass



class StorageAPIError(StorageError):
    pass



class Field(Renderable):

    name = None




class Storable(peewee.Model, Renderable):

    id = None
    name = None

    
    class Meta:
        database = db_proxy


    @staticmethod
    def load(id=None, name=None):

        if id is None and name is None:
            raise StorageException('Storable.load called with neither id nor name supplied.')

        if id is not None:
            try:
                id = int(id)
            except ValueError as e:
                raise StorageAPIError('Storable.load called with id value that is not castable to int. Original Error message: %s' % (e.message,))

            #TODO: Load by id

        #By this point, it is established that name has been passed.
        try:
            name = str(name)
        except ValueError as e:
            raise StorageAPIError('Storable.load called with name value that is not castable to str. Original Error Message: %s' % (e.message,))

        #TODO: Load by name
