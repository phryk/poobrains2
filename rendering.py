class Renderable(object):

    def render(mode='full'):
        
        tpls = [
            '%s-%s' % (self.__class__, mode),
            self.__class__,
        ]

        return render_template(tpls, {'obj': self})
