from flask import Flask, Blueprint
from peewee import SqliteDatabase, PostgresqlDatabase
from playhouse.db_url import connect

from rendering import Renderable
from storage import Storable, db_proxy



class Poobrains(Flask):

    database = None

    def __init__(self, *args, **kwargs):

        super(Poobrains, self).__init__(*args, **kwargs)

        self.site = Pooprint('site', 'poobrains', template_folder='templates/site')
        self.admin = Pooprint('admin', 'poobrains', template_folder='templates/admin', url_prefix='/admin')


    def run(self, *args, **kwargs):
        
        self.database = connect(app.config['DB_URL'])
        db_proxy.initialize(self.database)

        self.register_blueprint(self.site)
        self.register_blueprint(self.admin)

        super(Poobrains, self).run(*args, **kwargs)



class Pooprint(Blueprint):
    pass



app = Poobrains(__name__)
import defaults
import admin

app.admin.add_url_rule('/', view_func=admin.overview)
app.admin.add_url_rule('/install', view_func=admin.install)


@app.before_request
def db_connect():
    app.database.connect()


@app.after_request
def db_close(response):

    if not app.database.is_closed():
        app.database.close()

    return response
